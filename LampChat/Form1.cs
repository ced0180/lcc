﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SocketIOClient;
using System.Text.Json;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Net.WebSockets;
using System.Xml.Linq;
using System.Net.Sockets;

namespace LampChat
{
    public partial class Form1 : Form
    {
        SocketIO client;
        ChangeUser changeUser;

        public string uri = "https://xn--7ckep0i.tk";

        public string username = "c30.cs";
        public string usercolor = "#F64D0B";
        public string uuid = Guid.NewGuid().ToString();

        public class UserType
        {
            public string name { get; set; }
            public string color { get; set; }
            public string socketid { get; set; }
            public string uuid { get; set; }
        }

        public class MessageSend
        {
            public string content { get; set; }
            public FileSend file { get; set; }
        }

        public class MessageType
        {
            public string content { get; set; }
            public UserType user { get; set; }
            public string timestamp { get; set; }
            public FileType file { get; set; }
            public string _id { get; set; }
        }

        public class FileSend
        {
            public string type { get; set; }
            public string name { get; set; }
            public byte[] data { get; set; }
        }

        public class FileType
        {
            public string name { get; set; }
            public string type { get; set; }
        }

        public Form1()
        {
            InitializeComponent();

            changeUser = new ChangeUser();
            changeUser.Owner = this;

            client = new SocketIO(uri);

            client.On("messages", messages => {
                
            });

            client.On("users", _users => {
                //Console.WriteLine(_users);
            });


            client.On("message", message => {
                Console.WriteLine(message);

                MessageType msg = message.GetValue<MessageType>();
                Invoke(new Action(() =>
                {
                    richTextBox.SelectionColor = ColorTranslator.FromHtml(msg.user.color);
                    richTextBox.AppendText($"{Environment.NewLine}{msg.user.name}: { msg.content }");
                    if (msg.file != null) richTextBox.AppendText($"{Environment.NewLine}File({ msg.file.name }): { parseFileUrl(msg._id, msg.file.name) }");
                    richTextBox.SelectionColor = Color.FromArgb(255, 255, 255, 255);
                }));
            });

            client.OnConnected += (sender, e) =>
            {
                Invoke(new Action(() =>
                {
                    richTextBox.Clear();
                    richTextBox.SelectionColor = Color.FromArgb(255, 0, 255, 0);
                    richTextBox.AppendText("Connected");
                    richTextBox.SelectionColor = Color.FromArgb(255, 255, 255, 255);
                }));
                SetUser(username, usercolor, uuid);
            };

            client.OnDisconnected += (sender, e) =>
            {
                Invoke(new Action(() =>
                {
                    richTextBox.Clear();
                    richTextBox.SelectionColor = Color.FromArgb(255, 200, 0, 0);
                    richTextBox.AppendText("Disconnected");
                    richTextBox.SelectionColor = Color.FromArgb(255, 255, 255, 255);
                }));
            };

            Connect();
        }

        private async void Connect()
        {
            await client.ConnectAsync();
        }

        public async void SetUser(string name = "no name", string color = "#FFFFFF", string _uuid = null)
        {
            if (string.IsNullOrEmpty(_uuid)) _uuid = uuid;
            await client.EmitAsync("user", new UserType { name = name, color = color, uuid = _uuid});
            username = name;
            usercolor = color;
        }

        private async void SendMessage (string content = null, FileSend file = null)
        {
            if (!client.Connected) return;
            try
            {
                await client.EmitAsync("message", new MessageSend { content = content, file = file });
            }
            catch (Exception e)
            when (
                e is WebSocketException |
                e is ObjectDisposedException
            )
            {
                MessageBox.Show(e.Message, e.GetType().ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string parseFileUrl(string _id, string filename)
        {
            string url = Uri.EscapeUriString($"{ uri }/file/{ _id }/{ filename }");
            return url;
        }

        private async void Typing()
        {
            await client.EmitAsync("type");
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            Typing();

            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (string.IsNullOrEmpty(textBox.Text)) return;
                SendMessage(textBox.Text);
                textBox.Clear();
            }
        }

        private void uploadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult res = openFileDialog1.ShowDialog();
            if (res == DialogResult.OK)
            {
                Console.WriteLine(openFileDialog1.FileName);

                byte[] byteArray = File.ReadAllBytes(openFileDialog1.FileName);
                string mimeType = MimeMapping.GetMimeMapping(openFileDialog1.FileName);
                string content = string.IsNullOrEmpty(textBox.Text) ? null : textBox.Text;
                FileSend file = new FileSend
                {
                    type = mimeType,
                    name = openFileDialog1.SafeFileName,
                    data = byteArray
                };

                SendMessage(content, file);

                openFileDialog1.Reset();
                textBox.Clear();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if (menuItem == null) return;
            ContextMenuStrip calendarMenu = menuItem.Owner as ContextMenuStrip;
            if (calendarMenu == null) return;
            Control controlSelected = calendarMenu.SourceControl;
            if (controlSelected == null) return;

            switch (controlSelected)
            {
                case RichTextBox richTextBox:
                    richTextBox.SelectAll();
                    break;
                case TextBox textBox:
                    textBox.SelectAll();
                    break;
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if (menuItem == null) return;
            ContextMenuStrip calendarMenu = menuItem.Owner as ContextMenuStrip;
            if (calendarMenu == null) return;
            Control controlSelected = calendarMenu.SourceControl;
            if (controlSelected == null) return;

            switch (controlSelected)
            {
                case RichTextBox richTextBox:
                    richTextBox.Copy();
                    break;
                case TextBox textBox:
                    textBox.Copy();
                    break;
            }
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox.Cut();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox.Paste();
        }

        private void richTextBox_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            Process.Start(e.LinkText);
        }

        private void userToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeUser.textBox1.Text = username;
            changeUser.colorDialog1.Color = ColorTranslator.FromHtml(usercolor);
            changeUser.button2.BackColor = ColorTranslator.FromHtml(usercolor);
            changeUser.Show();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Environment.Exit(0);
        }

        private async void richTextBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (!client.Connected) return;
            decimal x = Convert.ToDecimal(e.X) / Convert.ToDecimal(richTextBox.Width);
            decimal y = Convert.ToDecimal(e.Y) / Convert.ToDecimal(richTextBox.Height);

            await client.EmitAsync("mouse", x, y);
        }
    }
}
